// ==UserScript==
// @name        JungleSurvey auto next
// @namespace   Violentmonkey Scripts
// @match       https://nightfallnews.com/category/*
// @grant       none
// @version     1.0
// @author      -
// @description 8/1/2020, 6:50:08 AM
// ==/UserScript==
(function() {
  setTimeout(() => {
    var nextBtn = document.getElementById('nextButton');

    nextBtn.scrollIntoView();
    nextBtn.click();
  }, 2000);
})();